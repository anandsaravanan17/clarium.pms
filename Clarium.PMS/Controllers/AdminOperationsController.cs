using Clarium.PMS.Models;
using Clarium.PMS.Service.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Clarium.PMS.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdminOperationsController : ControllerBase
    {

        private readonly ILogger<AdminOperationsController> _logger;
        private readonly IAdminOperationsService _adminOperationsService;

        public AdminOperationsController(ILogger<AdminOperationsController> logger, IAdminOperationsService adminOperationsService)
        {
            _logger = logger;
            _adminOperationsService = adminOperationsService;
        }
        [HttpPost(Name = "AddMasterDetails")]
        public bool AddDesignationsMaster(MasterDesignation masterDesignation)
        {
            return _adminOperationsService.AddDesignation(masterDesignation);
        }
        //[HttpGet(Name = "GetWeatherForecast")]
        //public IEnumerable<WeatherForecast> Get()
        //{
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
        //        TemperatureC = Random.Shared.Next(-20, 55),
        //        Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        //    })
        //    .ToArray();
        //}
    }
}
