﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Clarium.PMS.Models;

public partial class ClariumPmsContext : DbContext
{
    public ClariumPmsContext()
    {
    }

    public ClariumPmsContext(DbContextOptions<ClariumPmsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<MasterDesignation> MasterDesignations { get; set; }

    public virtual DbSet<MasterRole> MasterRoles { get; set; }

    public virtual DbSet<MasterScore> MasterScores { get; set; }

    public virtual DbSet<MasterSkill> MasterSkills { get; set; }

    public virtual DbSet<MasterStatusType> MasterStatusTypes { get; set; }

    public virtual DbSet<MasterTaskType> MasterTaskTypes { get; set; }

    public virtual DbSet<Project> Projects { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=VDI-1121-0\\SQLEXPRESS;Database=Clarium.PMS;Trusted_Connection=SSPI;encrypt=false;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MasterDesignation>(entity =>
        {
            entity.HasKey(e => e.DId).HasName("PK__Master_D__76B7E375FCB80F5D");

            entity.ToTable("Master_Designation");

            entity.Property(e => e.DId).HasColumnName("D_id");
            entity.Property(e => e.DDescription)
                .IsUnicode(false)
                .HasColumnName("D_Description");
            entity.Property(e => e.DTitle)
                .IsUnicode(false)
                .HasColumnName("D_Title");
        });

        modelBuilder.Entity<MasterRole>(entity =>
        {
            entity.HasKey(e => e.RoleId).HasName("PK__Master_R__D80BB0935526C3E7");

            entity.ToTable("Master_Roles");

            entity.Property(e => e.RoleId).HasColumnName("Role_id");
            entity.Property(e => e.RoleDescription)
                .IsUnicode(false)
                .HasColumnName("Role_Description");
            entity.Property(e => e.RoleTitle)
                .IsUnicode(false)
                .HasColumnName("Role_Title");
        });

        modelBuilder.Entity<MasterScore>(entity =>
        {
            entity.HasKey(e => e.ScoreId).HasName("PK__Master_S__289E850904AD4BD8");

            entity.ToTable("Master_Score");

            entity.Property(e => e.ScoreId).HasColumnName("Score_id");
            entity.Property(e => e.ScoreDescription)
                .IsUnicode(false)
                .HasColumnName("Score_Description");
            entity.Property(e => e.ScoreTitle)
                .IsUnicode(false)
                .HasColumnName("Score_Title");
        });

        modelBuilder.Entity<MasterSkill>(entity =>
        {
            entity.HasKey(e => e.RoleId).HasName("PK__Master_S__D80BB093E8B0454B");

            entity.ToTable("Master_Skills");

            entity.Property(e => e.RoleId).HasColumnName("Role_id");
            entity.Property(e => e.RoleDescription)
                .IsUnicode(false)
                .HasColumnName("Role_Description");
            entity.Property(e => e.RoleTitle)
                .IsUnicode(false)
                .HasColumnName("Role_Title");
        });

        modelBuilder.Entity<MasterStatusType>(entity =>
        {
            entity.HasKey(e => e.StatusId).HasName("PK__Master_S__5191052483D131BA");

            entity.ToTable("Master_StatusType");

            entity.Property(e => e.StatusId).HasColumnName("Status_id");
            entity.Property(e => e.StatusDescription)
                .IsUnicode(false)
                .HasColumnName("Status_Description");
            entity.Property(e => e.StatusTitle)
                .IsUnicode(false)
                .HasColumnName("Status_Title");
        });

        modelBuilder.Entity<MasterTaskType>(entity =>
        {
            entity.HasKey(e => e.TaskTypeId).HasName("PK__Master_T__7205F4EC2B8A4AA8");

            entity.ToTable("Master_TaskType");

            entity.Property(e => e.TaskTypeId).HasColumnName("Task_type_id");
            entity.Property(e => e.TaskTypeDescription)
                .IsUnicode(false)
                .HasColumnName("Task_type_Description");
            entity.Property(e => e.TaskTypeTitle)
                .IsUnicode(false)
                .HasColumnName("Task_type_Title");
        });

        modelBuilder.Entity<Project>(entity =>
        {
            entity.HasKey(e => e.ProjectId).HasName("PK__Project__761ABEF0F55D89D5");

            entity.ToTable("Project");

            entity.Property(e => e.CTime)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime")
                .HasColumnName("C_time");
            entity.Property(e => e.Description).IsUnicode(false);
            entity.Property(e => e.Name).IsUnicode(false);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
