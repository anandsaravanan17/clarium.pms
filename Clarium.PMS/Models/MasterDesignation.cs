﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class MasterDesignation
{
    public int DId { get; set; }

    public string? DTitle { get; set; }

    public string? DDescription { get; set; }
}
