﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class MasterRole
{
    public int RoleId { get; set; }

    public string? RoleTitle { get; set; }

    public string? RoleDescription { get; set; }
}
