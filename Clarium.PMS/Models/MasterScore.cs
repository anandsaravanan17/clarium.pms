﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class MasterScore
{
    public int ScoreId { get; set; }

    public string? ScoreTitle { get; set; }

    public string? ScoreDescription { get; set; }
}
