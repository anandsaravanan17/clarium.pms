﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class MasterSkill
{
    public int RoleId { get; set; }

    public string? RoleTitle { get; set; }

    public string? RoleDescription { get; set; }
}
