﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class MasterStatusType
{
    public int StatusId { get; set; }

    public string? StatusTitle { get; set; }

    public string? StatusDescription { get; set; }
}
