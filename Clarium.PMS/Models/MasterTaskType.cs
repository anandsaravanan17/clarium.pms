﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class MasterTaskType
{
    public int TaskTypeId { get; set; }

    public string? TaskTypeTitle { get; set; }

    public string? TaskTypeDescription { get; set; }
}
