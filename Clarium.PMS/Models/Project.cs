﻿using System;
using System.Collections.Generic;

namespace Clarium.PMS.Models;

public partial class Project
{
    public long ProjectId { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public DateTime? CTime { get; set; }
}
