using Clarium.PMS.Models;
using Clarium.PMS.Service;
using Clarium.PMS.Service.Interface;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddScoped<ClariumPmsContext>();
builder.Services.AddDbContext<ClariumPmsContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("connMSSQL")));

builder.Services.AddTransient<IAdminOperationsService, AdminOperationsService>();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
