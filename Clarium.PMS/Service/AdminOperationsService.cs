﻿using Clarium.PMS.Models;
using Clarium.PMS.Service.Interface;

namespace Clarium.PMS.Service
{
    public class AdminOperationsService : IAdminOperationsService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<AdminOperationsService> _logger;
        private readonly ClariumPmsContext _clariumPmsContext;
        public AdminOperationsService(IConfiguration configuration, ILogger<AdminOperationsService> logger, ClariumPmsContext clariumPmsContext)
        {
            _configuration = configuration;
            _logger = logger;
            _clariumPmsContext = clariumPmsContext;
        }
        public bool AddDesignation(MasterDesignation masterDesignation)
        {
            bool result = false;
            try
            {
                _clariumPmsContext.MasterDesignations.Add(masterDesignation);
                _clariumPmsContext.SaveChanges();
                result = true;
            }
            catch(Exception ex)
            {
                _logger.LogError("Exception Thrown : " + ex);
            }
            return result;
        }
    }
}
