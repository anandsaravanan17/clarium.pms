﻿using Clarium.PMS.Models;

namespace Clarium.PMS.Service.Interface
{
    public interface IAdminOperationsService
    {
        public bool AddDesignation(MasterDesignation masterDesignation);
    }
}
