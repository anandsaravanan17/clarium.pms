Create Table dbo.Master_Designation (
D_id integer primary key not null,
D_Title varchar(Max),
D_Description varchar(MAX)
);
Create Table dbo.Master_Roles (
Role_id integer primary key not null,
Role_Title varchar(Max),
Role_Description varchar(MAX)
);
Create Table dbo.Master_Skills (
Role_id integer primary key not null,
Role_Title varchar(Max),
Role_Description varchar(MAX)
);
create Table dbo.Master_TaskType(
Task_type_id integer primary key not null,
Task_type_Title varchar(Max),
Task_type_Description varchar(MAX)
);

create Table dbo.Master_StatusType(
Status_id integer primary key not null,
Status_Title varchar(Max),
Status_Description varchar(MAX)
);

create Table dbo.Master_Score(
Score_id integer primary key not null,
Score_Title varchar(Max),
Score_Description varchar(MAX)
);